
protoc -I ./proto \
   --go_out ./proto --plugin=protoc-gen-go=/Users/tuonglv/work/go/bin/protoc-gen-go --go_opt paths=source_relative \
   --go-grpc_out=require_unimplemented_servers=false:./proto --plugin=protoc-gen-go-grpc=/Users/tuonglv/work/go/bin/protoc-gen-go-grpc --go-grpc_opt paths=source_relative \
   --grpc-gateway_out ./proto --plugin=protoc-gen-grpc-gateway=/Users/tuonglv/work/go/bin/protoc-gen-grpc-gateway --grpc-gateway_opt paths=source_relative \
   ./proto/simulation/simulation.proto